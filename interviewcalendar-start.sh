docker rmi -f $(docker images -q registry.gitlab.com/pelsinho/interviewcalendar)
docker rmi -f $(docker images -q registry.gitlab.com/pelsinho/spring-boot-eureka-server)
docker rmi -f $(docker images -q registry.gitlab.com/pelsinho/spring-boot-admin-server)
docker rmi -f $(docker images -q registry.gitlab.com/pelsinho/spring-boot-config-server)
docker rmi -f $(docker images -q postgres:13.1-alpine)
cd interviewcalendar
docker-compose -f docker-compose-interview-calendar.yml up #-d