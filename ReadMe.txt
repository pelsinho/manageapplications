## Title

  Interview Schedule Calendar

## Technical Environment Rules

  Please implement the API, and skip UI. It’s also fine to skip the authentication.
  The purpose is not to build a solid API but for you to demonstrate your coding skills, architecture and engineering practices.

## Goal

  Build an interview calendar API.

  There may be two roles that use this API, a candidate and an interviewer.

  A typical scenario is when:

      1. An interview slot is a 1-hour period of time that spreads from the beginning of any
      hour until the beginning of the next hour. For example, a time span between 9am and
      10am is a valid interview slot, whereas between 9:30am and 10:30am it is not.

      2. Each of the interviewers sets their availability slots. For example, the interviewer
      David is available next week each day from 9am through 4pm without breaks and
      the interviewer Ingrid is available from 12pm to 6pm on Monday and Wednesday
      next week, and from 9am to 12pm on Tuesday and Thursday.

      3. Each of the candidates sets their requested slots for the interview. For example, the
      candidate Carl is available for the interview from 9am to 10am any weekday next
      week and from 10am to 12pm on Wednesday.

      4. Anyone may then query the API to get a collection of periods of time when it’s
      possible arrange an interview for a particular candidate and one or more interviewers.
      In this example, if the API is queries for the candidate Carl and interviewers Ines and
      Ingrid, the response should be a collection of 1-hour slots: from 9am to 10am on
      Tuesday, from 9am to 10am on Thursday.
      Remember: the purpose of this test is not to implement a production-ready API but to create
      basis for our further discussion.

  Writing tests!

## Tooling

  1. Please install IntelliJ or Eclipse Lombok plugin, to avoid possible compilation problems;
  2. Java 11;
  3. Maven;
  4. Docker and Docker-Compose;
  5. This application needs following ports available : 8080, 8090, 8761, 8888, 9411;

## Start Spring Cloud Environment

    1. Add this project in any computer directory
    2. Go to project root folder
    3. Run -> ./interviewcalendar-start.sh

        Info's :

            1. Run chmod +x interviewcalendar-start.sh , to set as executable file (Maybe it´s necessary)
            2. Remove -d flag in docker-compose command to run in quiet mode (no logs in terminal screen)

        Application Environment :

            1.  Main Application -> http://localhost:8080/swagger-ui.html#/calendar-controller
            2.  Spring Admin Server -> http://localhost:8090/ (user: admin / password: admin)
                Send an e-mail to ngomesgitlab@gmail.com when a services go up or down
            3.  Spring Configuration Server -> http://localhost:8888
            4.  Spring Eureka Server -> http://localhost:8761
            5.  ZipKin Server (Microservice Log Aggregator) -> http://localhost:9411/zipkin/
            6.  Test Environment : Postgres DB + Flyway
                Other Environments : H2 DB
            7.  Spring Native Support. (In Progress)
            8.  Parent POM (Phase 1)

        Application Git Environment : (Feel free to get any of them to check application environment code)

            1.  Main Application -> https://gitlab.com/pelsinho/interviewcalendar.git
            2.  Spring Cloud Admin Server -> https://gitlab.com/pelsinho/spring-boot-admin-server.git
            3.  Spring Cloud Configuration Server -> https://gitlab.com/pelsinho/spring-boot-config-server.git
            4.  Spring Configuration Files -> https://gitlab.com/pelsinho/spring.cloud.config.server.git.uri.git
            5.  Spring Cloud Eureka Server -> https://gitlab.com/pelsinho/spring-boot-eureka-server.git


# First Steps

  1. After start the application please go to SWAGGER UI Home Page (http://localhost:8080/swagger-ui.html) and read application rules, and information.
  2. You are also going to be able to test all application features from this Home Page.
  3. For a better user experience please take some minutes to read our instructions before start using it.

## Tech Stack

  1. Spring Framework

      - Spring Boot, JPA, Web, etc.
      - Please check POM for more information

  2. Lombok Plugin :

      - Reduce boiler plate code

  3. CheckStyle :

      - Code style pattern plugin (checkstyle.xml)

  4. Databases :

      Test Environment:

          H2

              - Url : http://localhost:8080/h2
              - User : sa
              - Password : sa

      Other Environment:

          Postgres + Flyway

              - DB : postgres
              - User : postgres
              - Password : password

  5. Jacoco :

      - Generate Test Reports to be used in Sonar or Jenkins.

  6. Maven Profiles : Dev and Prd

      - You can change default maven profile on pom.xml file, using <activeByDefault>true</activeByDefault>
      - or when you start application using -P flag in mvn command. Example : mvn install -P Prd

  7. Spring Profiles : Dev and Prd

      - You can switch on application.properties (spring.profiles.active), or adding a Env_Variable like : SPRING_PROFILE = PRD

  8. SQL Log :

      - Set as DEBUG for Spring Dev Profile
      - Set as INFO for Spring Prd Profile
      - You can switch on application.properties (logging.level.org.hibernate.SQL), or adding a Env_Variable like : SQL_LOG_LEVEL

  9. Control Advice :

      - Consolidate multiple, scattered @ExceptionHandlers from before into a single, global error handling component.

  10. Add git-commit-id-plugin dependency to the project, to get Code Info using /manage/info endpoint

## Tests

  1. Unit Tests :

    - Start Unit Tests using : mvn clean test
    - Folder : Src -> Main -> Test -> Java

  2. Functional Tests :

    - First Start Interview Application
    - Start Functional Tests using : mvn -Ptest failsafe:integration-test@api-tests failsafe:verify -Dgroups='functional' -Dspring-boot.run.profiles=test
    - Folder : Src -> Main -> Test -> Java -> functionaltests

    Please let me know if there is a problem running this command.

  3. Tests Support Classes :

    - Folder : Src -> Main -> Test -> Java -> testsupport


## GIT Lab

  If you want to login in to validate the project and the pipeline in GITLab you can use this login / password :

    1 - User : ngomesgitlab
    2 - Password : ngomesgitlab@1234

  I´ve setup a CI Pipeline to validate & control our Continuous Integration process.

  Phases :

    1 - Validate CheckStyle
    2 - Code Compilation
    3 -  Run Tests
      3.1 - JUnit Tests
      3.2 - Functional Tests (Need to be improved)
    4 - Generate Application Package
    5 - Deploy Docker Application in GitLab Registry


## Application Monitoring Environment (If needed)

    1. Add this project in any computer directory
    2. Go to project root folder

        Info's :

            1. Run chmod +x monitoring-start.sh , to set as executable file (Maybe it´s necessary)
            2. Run chmod +x logging-start.sh , to set as executable file (Maybe it´s necessary)
            3. Run chmod +x apm-elasticsearch-start.sh , to set as executable file (Maybe it´s necessary)
            3. Run chmod +x start-apm.sh on Main Application Folder, to set as executable file (Maybe it´s necessary)


        Kibana Info's (Docker Logs) :

            Run -> ./logging-start.sh

            1. Access Link -> http://localhost:5601
            2. Basic Configuration -> https://www.sarulabs.com/post/5/2019-08-12/sending-docker-logs-to-elasticsearch-and-kibana-with-filebeat.html
            3. Open an Select "Main Application View" filter to view "Main-Application" logs

        Grafana Info's (Application and DB Monitoring) :

            Run -> ./monitoring-start.sh

            1. Access Link -> http://localhost:3000
            2. Username : admin
            3. Password : admin

            At this moment we are using Grafana to :

                1. Monitor Containers
                2. Monitor Database
                3. Monitor Spring Application - Using Spring Actuator Prometheus Endpoint

        APM - Elastic Search - Info's (DB Queries Monitoring) :

            Step 1 : Start Application Environment, without Main Application

                cd interviewcalendar
                ./interviewcalendar-start-local.sh

            Step 2 : Start APM Elastic Search

                cd ..
                ./apm-elasticsearch-start.sh

            Step 3 : Start Main Application with APM Elastic Search Plug-In

                go to Main Application Folder
                ./start-apm.sh

            Info's

                1. Access Link -> http://localhost:5601/app/apm
                2. Info -> https://www.elastic.co/guide/en/apm/get-started/current/quick-start-overview.html


## How Clients should work with Eureka Server

  1. https://www.youtube.com/watch?v=GxLjcOE35oA&list=PLqq-6Pq4lTTZSKAFG6aCDVDP86Qx4lNas&index=21

## Design Patterns

  1. I like to use Chain of Responsibility Design Pattern during validation process to de-couple one validation from another and to avoid create a LOT of if´s.
