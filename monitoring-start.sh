#
# Start Grafana
#

docker rmi -f $(docker images -q grafana/grafana:latest)
docker rmi -f $(docker images -q prom/prometheus:latest)
docker rmi -f $(docker images -q google/cadvisor:latest)
docker rmi -f $(docker images -q prom/node-exporter:latest)
docker rmi -f $(docker images -q prom/alertmanager:latest)
docker rmi -f $(docker images -q wrouesnel/postgres_exporter:latest)

cd monitoring
cd grafana
sudo docker-compose -f docker-compose-grafana.yml up #-d