#
# Start Kibana
#

docker rmi -f $(docker images -q docker.elastic.co/apm/apm-server:7.13.4)
docker rmi -f $(docker images -q docker.elastic.co/kibana/kibana:7.13.4)
docker rmi -f $(docker images -q docker.elastic.co/elasticsearch/elasticsearch:7.13.4)

cd monitoring
cd apm-elasticsearch
docker-compose -f docker-compose-elasticsearch.yml up -d