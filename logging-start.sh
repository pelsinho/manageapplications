#
# Start Kibana
#

docker rmi -f $(docker images -q docker.elastic.co/beats/filebeat:7.2.0)
docker rmi -f $(docker images -q docker.elastic.co/kibana/kibana:6.6.1)
docker rmi -f $(docker images -q docker.elastic.co/elasticsearch/elasticsearch:6.6.1)

cd logging
cd kibana
docker-compose -f docker-compose-kibana.yml up -d