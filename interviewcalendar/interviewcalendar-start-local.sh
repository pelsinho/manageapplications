docker rmi -f $(docker images -q registry.gitlab.com/pelsinho/interviewcalendar)
docker rmi -f $(docker images -q registry.gitlab.com/pelsinho/spring-boot-eureka-server)
docker rmi -f $(docker images -q registry.gitlab.com/pelsinho/spring-boot-admin-server)
docker rmi -f $(docker images -q registry.gitlab.com/pelsinho/spring-boot-config-server)
docker rmi -f $(docker images -q postgres)
echo ----------------------------------------------------
echo - After this please start main application locally -
echo ----------------------------------------------------
docker-compose -f docker-compose-interview-calendar-local.yml up #-d