
# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.

export PATH="$PATH:$HOME/.rvm/bin"

#export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
#[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#export NVM_DIR="$HOME/.nvm"
#  [ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
#  [ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


export ZSH="/Users/nelsongomes/.oh-my-zsh"
ZSH_THEME="agnoster"
POWERLEVEL9K_MODE="awesome-patched"
DEFAULT_USER=`whoami`
autoload -U compinit
compinit

#Zsh
#ls='ls --color=tty'
grep='grep  --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'

alias -s {yml,yaml}=atom
alias -g G='| grep -i'  #  ls -l G do

d='dirs -v | head -10'
1='cd -'
2='cd -2'
3='cd -3'
4='cd -4'
5='cd -5'
6='cd -6'
7='cd -7'
8='cd -8'
9='cd -9'

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git zsh-autosuggestions docker sudo
)
source $ZSH/oh-my-zsh.sh


# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

#
# command ;
# command D
# command shift D
# command option setas
# ctrl command n




##############
# FULL START #
##############

alias fullstart='cdv && docdown && gohot && gojen'



########
# ATOM #
########

alias bashr='. ~/.zshrc'
alias atombas='atom ~/.zshrc'
alias atomdoc='cdv && atom docker-compose.yml'



#######
# DEV #
#######

alias devapi='cdv && ./dev vakt_api_up'
alias devapires='cdv && ./dev vakt_api_restart'
alias devapin='cdv && ./dev vakt_api_up with_non_vakt'

alias devbreins='cdv && ./dev brew install'

alias devcle='cdv && ./dev mvn_cleanx_all'
alias devcleall='cdv && ./dev mvn_clean_all vakt_api_up'

alias devfly='cdv && ./dev flyaway_clean_and_migrate'

alias devins='cdv && ./dev vakt_api_up with_inspector_api'
alias devinsres='cdv && ./dev inspector_api_restart'

alias devter='cdv && ./dev vakt_api_up with_terminal_api'

alias devref='./dev reference_data_api_restart'

alias devsto='cdv && ./dev stop_all'

alias devweb='cdv && ./dev web_restart'
alias devweblog='cdv && tail -f logs/npm.log'

alias devmulti='cdv && ./dev stop_all mvn_clean_all vakt_api_up with_multiple_vakt_api_entities'




##########
# CUSTOM #
##########

alias backupall='backup'
alias cdv="cd ~/projects/v-pod"
alias cdc="cd ~/projects/v-pod/image-vakt-tests/tests"
alias cda="cd ~/projects/v-pod/image-api/vakt-api"
alias cdi="cd ~/projects/v-pod/inspector-api/inspector-api"
alias code="open -a Visual\ Studio\ Code"
alias cfind="mdfind -onlyin . -name "
alias ll='ls -l'
alias ps1='ps aux | grep $1'
alias rm='rm -i'
alias top1='top | grep $1'


################
# DOCKER CLEAN #
################

alias docpru="docsyspru || docvolpru || docnetpru"
alias docsyspru="docker system prune -a"
alias docvolpru="docker volume prune"
alias docnetpru="docker network prune"
alias docrm="sudo docker rm -f $(docker ps -q -a)"
alias docrmi="sudo docker rmi -f $(docker images -q)"
alias docdev="docker rmi -f $(docker images --filter "dangling=true" -q --no-trunc)"



###############
# DOCKER BASH #
###############

alias apiexe='docker exec -it $(docker ps -aqf "name=v-pod_api_1") /bin/bash'
alias dbexe='docker exec -it $(docker ps -aqf "name=v-pod_db_1") /bin/bash'
alias docexe='docker exec -i -t  $1  /bin/bash'
alias msgexe='docker exec -it $(docker ps -aqf "name=v-pod_message_queue_1") /bin/bash'



###################
# DOCKER COMMANDS #
###################

alias docpsa1="docker ps -a --format 'table {{.ID}}\t{{.Image}}\\t.{{.Command}}\t.{{.Names}}' | grep $1"
alias docpsa="docker ps -a --format 'table {{.ID}}\t{{.Image}}\\t.{{.Command}}\t.{{.Names}}'"
alias docps1="docker ps --format 'table {{.ID}}\t{{.Image}}\\t.{{.Command}}\t.{{.Names}}' | grep $1"
alias docps="docker ps --format 'table {{.ID}}\t{{.Image}}\\t.{{.Command}}\t.{{.Names}}'"
alias docsta='docker stats'

################
# DOCKER FIXES #
################

alias docqorfix="rm -rf .cache/dir_hashes/image_quorum.hash"


############
# DOC LOGS #
############

function doclog() {
  cdv
  docker logs v-pod_api_1 -f
}

function doclogval() {
  cdv
  echo $1
  docker logs v-pod_api_1 -f | grep -iw $1
}

function doclogref {
  cdv
  docker logs v-pod_reference_data_api_1  -f
}

function doclogrefval() {
  cdv
  echo $1
  docker logs v-pod_reference_data_api_1  -f | grep -iw $1
}



#############
# TRADES V2 #
#############

alias tv2='cd ~/linux-ngomes/Comp4/spikes/MCP_barges/samplePayloads && ./load_trades_set1_localhost.sh shell && ./load_trades_set1_localhost.sh gunvor'





###############
# DOC RESTART #
###############

alias apires='docker restart v-pod_api_1'
alias conres='docker restart v-pod_contract_1'
alias dbres="cdv && gofly && goref && gocon"
alias q1res='docker restart v-pod_quorum_1_1'
alias q2res='docker restart v-pod_quorum_2_1'
alias q3res='docker restart v-pod_quorum_3_1'
alias quores="q1res | q2res"
alias quo3res="q1res | q2res | q3res"
alias docres="osascript -e 'quit app \"Docker\"' && open -a Docker"



#######
# GIT #
#######

#alias checkout='git checkout $1'
#alias checkoutb='git checkout -b $1'
# alias comitapi='cdv && git add image-api/ && git commit -m $1'
# alias comitapi='cdv && git add image-web/ && git commit -m $1'
# alias gitpus='cdv && masterRest && git push --set-upstream origin'
# alias gitpusf='cdv && masterRest && git push --set-upstream origin --force'
# alias gitreba='cdv && masres && git pull origin master --rebase'
alias gitbra='git branch | grep \* | cut -d ' ' -f2'
alias gitfet='git fetch'
alias gitresjso='cdv && git checkout -- image-web/package-lock.json'
alias gitres='cdv && masres'
alias gitsta='git status'
alias gitpusall='masterRest && git push --set-upstream origin --force'
alias gitreb='masres && git pull origin master --rebase'
alias gitrebtango='masres && git pull origin env-tango-v4.5.1 --rebase'
alias gitrebc='git rebase --continue'
alias gitreba='git rebase --abort'
alias gitreset='gitresetorigin'
alias gitpus='gitpus'
alias gitpusf='gitpusf'
alias githard='git reset --hard origin/master'

function gitche() {
  cdv
  git checkout $1
}

function gitcheb() {
  cdv
  git checkout -b $1
}

function gitcomweb() {
  cdv
  gitres
  git add image-web/
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git commit -m '[NELSON]['$var']'" $@"
}

function gitpusweb() {
  cdv
  gitres
  git add image-web/
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git commit -m '[NELSON]['$var']'" $@"
  git push --set-upstream origin --force
}

function gitcomapi() {
  cdv
  gitres
  git add image-api/
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git commit -m '[NELSON]['$var']'" $@"
}

function gitpusapi() {
  cdv
  gitres
  git add image-api/
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git commit -m '[NELSON]['$var']'" $@"
  git push --set-upstream origin --force
}

function gitpus() {
  cdv
  gitres
  git add image-api/
  git add image-web/
  git add core-api/
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git commit -m '['$var'][NELSON]'" $@" --no-verify
  git push --set-upstream origin --no-verify
}

function gitpusp() {
  cdp
  git add .
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git commit -m '[NELSON]['$var']'" $@" --no-verify
  git push --set-upstream origin
}

function gitresetorigin() (
  cdv
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git reset --hard origin/$var
)

function gitpusc() {
  cdc
  git add .
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git commit -m '[NELSON]['$var']'" $@" --no-verify
  git push --set-upstream origin
}

function gitpuscf() {
  cdc
  gitres
  git add .
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git commit -m '[NELSON]['$var']'" $@" --no-verify
  git push --set-upstream origin --force
}

function gitpuspf() {
  cdp
  gitres
  git add .
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git commit -m '[NELSON]['$var']'" $@" --no-verify
  git push --set-upstream origin --force
}

function gitpusf() {
  cdv
  gitres
  git add image-api/
  git add image-web/
  git add core-api/
  var=$(git branch | grep \* | cut -d ' ' -f2)
  git commit -m '['$var'][NELSON]'" $@" --no-verify
  git push --set-upstream origin --force --no-verify
}

################
# CUCUMBER     #
# @admin       #
# @cargo       #
# @trades      #
# @permissions #
################-Dweb.name=headless -Plocal

alias cucsmo='cucsmof'
alias cuccar='cucscarf'
alias cuctra='cdc && mvn clean install -Dcucumber.options='--tags "@trades and not @barges and not @nominations"''
alias cucsmorelint='cdc && mvn clean install -Dcucumber.options='--tags "@smoke and not @barges and not @nominations"''
alias cuccarrelint='cdc && mvn clean install -Dcucumber.options='--tags "@cargo and not @barges and not @nominations"''
alias cuctrarelint='cdc && mvn clean install -Dcucumber.options='--tags "@trades and not @barges and not @nominations"''
alias cucnom='cdc && mvn clean install -Dcucumber.options='--tags "@nominations and not @barges and not @trades"''
alias cucbar='cdc && mvn clean install -Dcucumber.options='--tags "@barges and not @nominations and not @trades"''

#V2 TRADES
alias cuclogtranonvakt='cdc && mvn verify -Dcucumber.options='--tags "@send-nomination-for-unpaired-trade-with-nonvakt"''
alias cuclogtravakt='cdc && xmvn verify -Dcucumber.options='--tags "@auto-pair-trades-using-brokerid-and-legs-exchangeid-with-paired-legs-on-trade-creation and @rest"''

alias cuctrabemails='cdc && mvn clean install -Dcucumber.options='--tags "@non-vakt"''
alias cuctrabfoet='cdc && mvn clean install -Dcucumber.options='--tags "@bfoet-trades and not @barges and not @nominations"''
alias cuccarbfoet='cdc && mvn clean install -Dcucumber.options='--tags "@cargo-chain and not @barges and not @nominations"''
# @bfoet-trades #
# @cargo-chain #


cucsmof() {
  cdc
  mvn clean install -Dcucumber.options='--tags "@smoke and not @barges and not @nominations"'
}

cucscarf() {
  cdc
  mvn clean install -Dcucumber.options='--tags "@cargo and not @barges and not @nominations"'
}



############
# DATABASE #
############
alias dbdow='sudo -u postgres /Library/PostgreSQL/10/bin/pg_ctl -D /Library/PostgreSQL/10/data/ stop'



#########
# MAVEN #
#########

alias mvncle='mvn clean -DskipTests'
alias mvnins='mvn clean install -DskipTests'
alias mvndep='mvn clean deploy -DskipTests'
alias mvnapi='cda && mvn install -Dmaven.test.skip=true -DskipTests -Djacoco.skip=true -P hotdeploy'


################
# PROJECT VPOD #
################

alias goapi="cda && mvnapi && apires && monApiSta"
alias gocon="cdv && docker-compose up contract_1"
alias gofly='cdv && docker-compose up -d flyway_1 flyway_2'
alias gofly3='cdv && docker-compose up -d flyway_1 flyway_2 flyway_3'
alias gohota="cdv && ./go populate_address_book && ./go local_hot_up"
alias gohot="cdv && ./go local_hot_up"
alias gohotna="cdv && ./go populate_address_book && ./go local_hot_up_nonvakt"
alias gohotn="cdv && ./go local_hot_up_nonvakt"
alias goinsp="cdv && ./go inspectors_local_up"
alias goref="vpod-refdata"
alias goreft='cdv && docker-compose up -d refdata-import_1  refdata-import_2'
alias gores="cdv && ./go local_down && vpod-docker-restart && sleep 50"
alias gosed="cdv && ./go local_seed"
alias docup="cdv && ./go local_up"
alias docdown="cdv && cd ./image-api/hotdeploy/ && ./DockerHotDown.sh"
alias docdownd="cdv && ./go local_down"
alias docdowni="cdv && ./go inspectors_local_down"


#MONITORING
#alias graup="cdv && docker-compose -f docker-compose-monitoring.yml up"
#alias gradow="cdv && docker-compose -f docker-compose-monitoring.yml down "
alias graup='graupf'
alias gradow='gradowf'
alias kibup='kibupf'
alias kibdow='kibdowf'
#monitoring_prometheus
#monitoring_cadvisor
# -d --force-recreate --remove-orphans

gradowf() {
  cdv
  cd monitoring
  cd grafana
  docker-compose -f docker-compose-grafana.yml down
  cdv
}

graupf() {
  cdv
  cd monitoring
  cd grafana
  docker-compose -f docker-compose-grafana.yml up -d
  cdv
}

kibdowf() {
  cdv
  cd monitoring
  cd kibana
  docker-compose -f docker-compose-kibana.yml down
  cdv
}

kibupf() {
  cdv
  cd monitoring
  cd kibana
  docker-compose -f docker-compose-kibana.yml up -d
  cdv
}

#######
# URL #
#######

url() {
 url=$([[ $1 =~ ^[a-zA-Z]{1,}: ]] && printf '%s\n' "$1" || printf '%s\n' "http://$1")
 open -a 'Google Chrome' "$url"
}

urlh() {
 url=$([[ $1 =~ ^[a-zA-Z]{1,}: ]] && printf '%s\n' "$1" || printf '%s\n' "https://$1")
 open -a 'Google Chrome' "$url"
}

alias urlshe='urlh http://shell.vakt.com:8080/'
alias urlgun='urlh http://gunvor.vakt.com:8080/'
alias urlmai='url http://localhost:1080'
alias utlgit='urlh https://github.com/v-squad/v-pod/commits/master'
alias urlglo='url https://www.globo.com/'
alias urlfac='urlh https://www.facebook.com/'


#######
# MONITOR IF APPLICATION STARTS AND READY #
#######

alias cheQuo='monQuoStaApi'

function monQuoStaApi(){
  echo "---------------------------------------"
  echo "20 seconds for First Quorum Vallidation"
  echo "---------------------------------------"
  timespendQuorum=0
  sleep 20s
  varConSuc="OK"
  echo "----------------------------------------"
  echo "Set Contracts Validation process to: " $varConSuc
  echo "----------------------------------------"
  retryVaktQuorum
}

function retryVaktQuorum(){

  varConSuc="OK"
  varSucQ1="$(docker logs --tail 150 v-pod_quorum_1_1 -t G '200 OK')"
  varSucQ2="$(docker logs --tail 150 v-pod_quorum_2_1 -t G '200 OK')"
  varErrQ1="$(docker logs --tail 20 v-pod_quorum_1_1 -t G '500 Internal Server')"
  varErrQ2="$(docker logs --tail 20 v-pod_quorum_2_1 -t G '500 Internal Server')"
  expSucURL="200 OK"
  expErrURL="500 Internal Server"

  if [[ ("$varErrQ1" == *"$expErrURL"*) && ("$varSucQ1" != *"$expSucURL"*) ]] ; then
      echo "----------------------------------------------------"
      echo "Erro Q1 - Restarting Quroum1"
      echo "Please check if need to run: rm -rf work/data/quorum"
      varConSuc="NOK"
      echo "Set Contracts Validation process to: " $varConSuc
      echo "----------------------------------------------------"
      q1res
  fi

  if [[ ("$varErrQ2" == *"$expErrURL"*) && ("$varSucQ2" != *"$expSucURL"*) ]] ; then
      echo "----------------------------------------------------"
      echo "Erro Q2 - Restarting Quroum2"
      echo "Please check if need to run: rm -rf work/data/quorum"
      varConSuc="NOK"
      echo "Set Contracts Validation process to: " $varConSuc
      echo "----------------------------------------------------"
      q2res
  fi

  if [[ ("$varSucQ1" == *"$expSucURL"*) && ("$varSucQ2" == *"$expSucURL"*) ]] ; then
    echo "-------------------------------------"
    echo "Quroum 1 and 2 Started"
    echo "Starting Monitoring Image-Api Process"
    echo "-------------------------------------"
    if [[ ("$varConSuc" == *"NOK"*) ]] ; then
        echo "---------------------------------------------------"
        echo "Detected an Erro in Q1 or Q2 - Restarting Contracts"
        echo "Contracts Validation process set to: " $varConSuc
        echo "---------------------------------------------------"
        conres
    fi
    monApiSta
  else
    echo "-------------------------------------------------------"
    echo "Quorum Starting, please wait, Total Time in sec: " $timespendQuorum
    echo "Please check container logs if takes more than 250 sec "
    echo "-------------------------------------------------------"
    timespendApi=$((timespendQuorum+10))
    sleep 10s
    retryVaktQuorum
  fi
}

alias cheApi='monApiSta'

function monApiSta(){
  echo "------------------------------------------"
  echo "20 seconds for First Image-Api Vallidation"
  echo "------------------------------------------"
  timespendApi=0
  sleep 20s
  retryVaktApi
}

function retryVaktApi(){
  #arDoc="$(docker logs --tail 50 v-pod_api_1 | grep  'Completed initialization')"
  #varCurl2="$(curl -f http://shell.vakt.com:8080/v0/status G 'OK')"
  varCurl="$(curl -f -s http://shell.vakt.com:8080/v0/status/apihealth G "OK")"
  #expDocLog="ok"
  expcURL="OK"
  if [[ "$varCurl" == *"$expcURL"* ]] ; then
     echo "------------------------------------------------"
     echo "Image-Api Started"
     echo "Openning Shell Vakt Application in Google Chrome"
     echo "------------------------------------------------"
     urlshe
   else
    echo "-------------------------------------------------------"
    echo "Image-Api Starting, please wait, Total Time in sec: " $timespendApi
    echo "Please check container logs if takes more than 300 sec "
    echo "-------------------------------------------------------"
    timespendApi=$((timespendApi+10))
    sleep 10s
    retryVaktApi
   fi
}





createcontainers(){
docker commit v-pod_mail_1_1 v-pod_mail_1_1_image
docker commit v-pod_message_queue_1 v-pod_message_queue_1_image
docker commit v-pod_quorum_1_1 v-pod_quorum_1_1_image
docker commit v-pod_quorum_2_1 v-pod_quorum_2_1_image
}


backup(){
sudo cp ~/.bashrc_vakt ~/projects/jenkins/backup/bashrc_vakt
sudo cp ~/.zshrc ~/projects/jenkins/backup/zshrc
sudo cp -R ~/projects/local-env/rake ~/projects/jenkins/backup
#sudo cp -R ~/Projects/local-env/docker-compose ~/Projects/jenkins/backup
#sudo cp -R /Library/PostgreSQL/10/share ~/Projects/jenkins/backup
}






dbPorts(){

echo "Process 5432"
echo "Process 5432"
echo "Process 5432"
lsof -i tcp:5432
echo "Process 5433"
echo "Process 5433"
echo "Process 5433"
lsof -i tcp:5433
}


masres(){
cdv
#git checkout -- docker-compose.yml
git checkout -- image-web/package-lock.json
#rm image-flyway/migrations/vakt-api/V999__Populate_Default_Address_Book_Table.sql
}





#BACKUP
alias vpod-build="cd ~/projects/v-pod/image-api/vakt-api && ./mvnw -Dmaven.repo.local=~/v-squad/v-pod/image-api/vakt-api/.mvn/repository clean verify"
alias vpod-stop="cdv && ./go local_down && vpod-docker-restart && sleep 50"
alias vpod-start="cdv && ./go local_up"
alias vpod-seed="cdv && ./go local_seed"
alias vpod-api-restart="cdv && docker stop v-pod_api_1 && ./go disable_auth image:api:build local:docker_compose:up"
alias vpod-contract-restart="cdv && docker-compose restart contract_1"
alias vpod-fly='cdv && docker-compose restart flyway_1 && docker-compose restart flyway_2 && docker-compose restart refdata-import_1 && docker-compose restart refdata-import_2'
alias vpod-key='docker restart $(docker ps -aqf "name=v-pod_keycloak_1")'
alias vpod-docker-restart="osascript -e 'quit app \"Docker\"'; open -a Docker"
alias vpod-log-db='docker logs -f $(docker ps -aqf "name=v-pod_flyway_1_1")'
alias vpod-log-api-1='docker logs -f $(docker ps -aqf "name=v-pod_api_1")'
alias vpod-clean-db="rm -rf ~/v-squad/v-pod/postgres-data/shared"
alias vpod-db='docker exec -it $(docker ps -aqf "name=v-pod_db_1") /bin/bash'
alias vpod-api-1='docker exec -it $(docker ps -aqf "name=v-pod_api_1") /bin/bash'
alias vpod-rabbit='docker exec -it $(docker ps -aqf "name=v-pod_message_queue_1") /bin/bash'
alias vpod-clean-docker="docker system prune -a"
alias vpod-clean-docker-volume="docker volume prune"
alias vpod-clean-docker-network="docker network prune"

alias dockerApiLog='docker logs -f v-pod_api_1'
alias dockerApiLg='docker logs -f v-pod_api_1 &> /Users/nelsongomes/linux-ngomes/logs/dockerApiLog &'
alias dockerApiRest='cdv && docker stop v-pod_api_1 && rake disable_auth image:api:build local:docker_compose:up'
alias dockerApiRestart1='docker container restart v-pod_api_1'
alias dockerApiRestart3='docker-compose -f ~/projects/v-pod/docker-compose.yml up -d api'
alias dockerApiRestart4='vpod-api-restart'





############
# DATABASE #
############
#alias dbpor='dbPorts'
#alias dbkil='npx kill-port 5432 && npx kill-port 5433'
#alias dbsha='sudo cp -R ~/Projects/v-pod/postgres-data/shared/ /Library/PostgreSQL/10/share/'
#alias dbdel='cd /Library && sudo rm -rf PostgreSQL/10'
#alias godb='sudo -u postgres /Library/PostgreSQL/10/bin/pg_ctl -D /Library/PostgreSQL/10/data/ start'





###############
# DOC COMPOSE #
###############
#alias comfly='cdv && cp ../local-env/docker-compose/withFlyway/docker-compose.yml . && git checkout -- Rakefile '
#alias comflyn='cdv && cp ../local-env/docker-compose/withFlywayN/docker-compose.yml . && git checkout -- Rakefile . '
#alias comnofly='cdv && cp ../local-env/docker-compose/withoutFlyway/docker-compose.yml . && cp ../local-env/rake/noFly/Rakefile .'
#alias comnoflyn='cdv && cp ../local-env/docker-compose/withoutFlywayN/docker-compose.yml . && cp ../local-env/rake/noFly/Rakefile .'
#alias comhot='echo "Update DockerCompose - HotDeploy" && cdv && cp ../local-env/docker-compose/hot/docker-compose.yml .'
#alias comhotn='echo "Update DockerCompose - HotDeploy - Gunvor NonVakt" && cdv && cp ../local-env/docker-compose/hotNonVakt/docker-compose.yml .'
#alias comres='cdv && git checkout -- docker-compose.yml && git checkout -- Rakefile'
#alias flyAB='cdv && ./go populate_address_book'



###############
# DOCKER SYNC #
###############
#alias syncstart='cdv && dockersync'
#dockersync(){
#
#  cdv
#  cp ../local-env/docker-compose/docker-sync/docker-compose-dev.yml .
#  cp ../local-env/docker-compose/docker-sync/docker-sync.yml .
#  [ ! "$(docker ps -a | grep eugenmayer/unison)" ] && docker pull eugenmayer/unison
#  docker-sync-stack start
#
#}


# Key Store Commands
# keytool -list -v -keystore $JAVA_HOME/jre/lib/security/cacerts
# System.setProperty("javax.net.ssl.trustStore",$JAVA_HOME/jre/lib/security/cacerts);
# https://stackoverflow.com/questions/5871279/java-ssl-and-cert-keystore
# top
# keytool -list -v -keystore /app/keycloack.jks
# https://github.com/v-squad/v-pod/pull/2267#pullrequestreview-364248880
#http://thesecretlivesofdata.com/raft/



###########
# JENKINS #
###########
#jenkinsProcess(){
#
#echo 'Stop Jenkins Image'
#docker stop $(docker ps  --filter="ancestor=jenkins" -q)
#echo 'UpdateLocalIp in config.sh'
#cd ~/projects/jenkins/jenkins/apps/updateconfigu
#echo ./updateLocalIp.sh
#echo 'Start Jenkins Image'
#cd ~/projects/jenkins/docker
#./startJenkinsDocker.sh
#cdv
#
#}

###########
# JENKINS #
###########

#alias gojen='jenkinsProcess'
#alias jenexe='docker exec -i -t  $(docker ps  --filter="ancestor=jenkins" -q)  /bin/bash'
#alias jensta='docker start $(docker inspect --format='{{.Name}}' $(docker ps  --filter="ancestor=jenkins" -q))'
#alias jenres='docker restart $(docker inspect --format='{{.Name}}' $(docker ps  --filter="ancestor=jenkins" -q))'
#alias jendown='docker stop $(docker inspect --format='{{.Name}}' $(docker ps  --filter="ancestor=jenkins" -q))'
#alias atomjencon='atom ~/Projects/jenkins/jenkins/apps/updateconfigu/config.sh '
#alias atomjendoc='atom ~/Projects/jenkins/docker/Dockerfile'
#alias atomjensta='atom ~/Projects/jenkins/docker/startJenkinsDocker.sh'



###########
# RUBOCOP #
###########
# alias rcop='cdv && rubocop'
# alias rcopa='cdv && rubocop -a'
# alias rcopd='cdv && rubocop --display-only-fail-level-offenses'
# alias rcopc='cdv && RSPEC_COVERAGE=true rspec'
# alias rcopt='cdv && rspec'



##################
# RUN CONTAINERS #
##################
#runcontainers(){
#
#[ ! "$(docker ps -a | grep v-pod_mail_1_1_image)" ] &&  docker run -d --rm -it v-pod_mail_1_1_image && echo "Mail Starting"
#[ "$(docker ps -a | grep v-pod_mail_1_1_image)" ] &&  echo "Mail Running"
#
#[ ! "$(docker ps -a | grep v-pod_message_queue_1_image)" ] &&  docker run -d --rm -it v-pod_message_queue_1_image && echo "Queue Starting"
#[ "$(docker ps -a | grep v-pod_message_queue_1_image)" ] &&  echo "Queue Running"
#
#[ ! "$(docker ps -a | grep v-pod_quorum_1_1_image)" ] &&  docker run -d --rm -it v-pod_quorum_1_1_image && echo "Quorum 1 Starting"
#[ "$(docker ps -a | grep v-pod_quorum_1_1_image)" ] &&  echo "Quorum 1 Running"
#
#[ ! "$(docker ps -a | grep v-pod_quorum_2_1_image)" ] &&  docker run -d --rm -it v-pod_quorum_2_1_image && echo "Quorum 2 Starting"
#[ "$(docker ps -a | grep v-pod_quorum_2_1_image)" ] &&  echo "Quorum 2 Running"
#
#}






# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder



# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

#docker-compose -f docker-compose-multiple_legal_entities.yml up -d

source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
